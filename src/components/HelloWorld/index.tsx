import React from "react";

export interface HelloWorldInterface {
    textToShow: string
}

const HelloWorld = (props: HelloWorldInterface): JSX.Element => {
    const {textToShow} = props;
    return <p>{textToShow}</p>;
};

export default HelloWorld;
