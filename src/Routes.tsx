import React, {Suspense, lazy} from "react";
import {Switch, Route} from "react-router-dom";

const Homepage = lazy(() => import('./views/Homepage'));
import LoadingHomePage from "./components/LoadingHomePage";
const Routes = () => {
    return (
        <Switch>
            <Route exact path={'/'}>
                <Suspense fallback={<LoadingHomePage/>}>
                    <Homepage />
                </Suspense>
            </Route>
        </Switch>
    )
};

export default Routes;
