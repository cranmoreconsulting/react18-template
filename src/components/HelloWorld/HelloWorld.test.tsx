import React from 'react';
import { shallow } from '../../tests/test-setup';
import HelloWorld, {HelloWorldInterface} from "./index";

describe('Testing HelloWorld Component', () => {
  const initialProps = {
    textToShow: "HomeTest"
  } as HelloWorldInterface;
  const container = shallow(<HelloWorld {...initialProps} />);

  it('Should match the snapshot', () => {
    expect(container.html()).toMatchSnapshot();
  });

  it('Should contain the text "HomeTest"', () => {
    expect(container.text().includes('HomeTest')).toBe(true);
  });

  it('Should contain the text passed in props"', () => {
    expect(container.text().includes(initialProps.textToShow)).toBe(true);
  });

  it('Should not contain the text "Loading"', () => {
    expect(container.text().includes('Loading')).toBe(false);
  });

});