import React from "react";

const LoadingHomePage = (): JSX.Element => {
    return <div style={{color: 'red'}}>Loading...</div>;
};

export default LoadingHomePage;
