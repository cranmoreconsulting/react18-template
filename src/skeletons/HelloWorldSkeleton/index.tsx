import React from "react";
import Skeleton from "react-loading-skeleton";

const HelloWorldSkeleton = (): JSX.Element => {
    return <Skeleton count={2} width={100} />;
};

export default HelloWorldSkeleton;
