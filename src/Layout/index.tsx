import React from "react";
import "./Layout.css";

const Layout = (props: any) => {
  const {children} = props;
  return (
    <div className={'ApplicationContainer'}>
      {children}
    </div>
  );
};

export default Layout;