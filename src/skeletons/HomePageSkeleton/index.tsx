import React from "react";
import Skeleton from "react-loading-skeleton";

const HomePageSkeleton = (): JSX.Element => {
    return <Skeleton count={3} width={100}/>;
};

export default HomePageSkeleton;
