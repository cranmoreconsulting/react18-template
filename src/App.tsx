import React from "react";
import { BrowserRouter as Router} from 'react-router-dom';
import Routes from "./Routes";
import Layout from "./Layout";

const App = () => {
    return (
      <Layout>
        <Router>
            <Routes/>
        </Router>
      </Layout>
    )
};

export default App;
