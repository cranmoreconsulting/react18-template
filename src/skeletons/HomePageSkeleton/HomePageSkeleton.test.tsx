import React from 'react';
import { shallow } from '../../tests/test-setup';
import LoadingHomePage from "./index";

describe('Testing LoadingHomePage Component', () => {
  const container = shallow(<LoadingHomePage />);

  it('Should match the snapshot', () => {
    expect(container.html()).toMatchSnapshot();
  });
});