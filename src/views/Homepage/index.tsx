import React, {lazy, Suspense, useEffect, useState} from "react";
import * as Realm from "realm-web";
import './Homepage.css';
import LoadingHomePage from "../../components/LoadingHomePage";

const HelloWorld = lazy(() => import('../../components/HelloWorld'));

const Homepage = (): JSX.Element => {
  const app = new Realm.App({ id: "livedatasync-vrmad" });

  const [realmConnection, setRealmConnection] = useState();

  const [dataToDisplay, setDataToDisplay] = useState([]);

  const [nameValue, setNameValue] = useState('');
  const [statusValue, setStatusValue] = useState('');

  const onAddRealm = () => {
    const newItem = {
      name_of_data: nameValue,
      status_of_data: statusValue
    }
    // @ts-ignore
    realmConnection?.insertOne(newItem)
      .then((result: { insertedId: any; }) => console.log(`Successfully inserted item with _id: ${result.insertedId}`))
      .catch((err: any) => console.error(`Failed to insert item: ${err}`))
  };

  const [fieldToQuery, setFieldToQuery] = useState('');
  const [valueToQuery, setValueToQuery] = useState('');

  const newQuery = () => {
    const query = { [fieldToQuery]: valueToQuery };
    console.log(query)
    // @ts-ignore
    realmConnection?.find(query).then((dataReturned) => {
      console.log(dataReturned)
      setDataToDisplay(dataReturned)
    })
  }

  const GetLiveData = () => {
    app.logIn(Realm.Credentials.anonymous()).then(() => {
      const mongodb = app?.currentUser?.mongoClient("mongodb-atlas");
      const dataTest = mongodb?.db("Dean")?.collection("Data Sync Test");
      // @ts-ignore
      setRealmConnection(dataTest)

      const query = { "status_of_data": "live" };
      return dataTest?.find(query).then((dataReturned) => {
        // @ts-ignore
        setDataToDisplay(dataReturned)
      })
    });
  };

  useEffect(() => {
    GetLiveData()
  }, [])

  return (
    <div>
      <h1>Homepage</h1>

      <Suspense fallback={<LoadingHomePage/>}>
        <HelloWorld textToShow={'Hello'}/>
        <HelloWorld textToShow={'World'}/>
        <h2>Realm</h2>

        <div>
          <h3>Add</h3>
          <input name={'Name Of Data'} type={'text'} placeholder={'Name'} value={nameValue} onChange={(e) => setNameValue(e.target.value)}/>
          <br/>
          <input name={'Status Of Data'} type={'text'} placeholder={'Status'} value={statusValue} onChange={(e) => setStatusValue(e.target.value)}/>
          <br/>
          <button onClick={onAddRealm}>Add To Realm</button>
        </div>

        <div>
          <h3>Query</h3>
          <input name={'Field To Query'} type={'text'} placeholder={'Field To Query'} value={fieldToQuery} onChange={(e) => setFieldToQuery(e.target.value)}/>
          <br/>
          <input name={'Value To Query'} type={'text'} placeholder={'Value To Query'} value={valueToQuery} onChange={(e) => setValueToQuery(e.target.value)}/>
          <br/>
          <button onClick={newQuery}>Query Realm</button>
        </div>


        <div>
          <h3>View Data</h3>
          <table className={'zui-table'}>
            <thead>
            <tr>
              {dataToDisplay[0] !== undefined && Object.keys(dataToDisplay[0]).map((value, index) => {
                return (
                  <td key={index}>{value}</td>
                )
              })}
            </tr>
            </thead>
            <tbody>
            {dataToDisplay.map((dataRow, dataIndex) => {
              return (
                <tr key={dataIndex}>
                  {dataToDisplay[0] !== undefined && Object.keys(dataToDisplay[0]).map((key, keyIndex) => {
                    if(key === "_id")
                      // @ts-ignore
                      return <td key={keyIndex}>{dataRow[key].toHexString()}</td>
                    else
                      return <td key={keyIndex}>{dataRow[key]}</td>
                  })}
                </tr>
              )
            })}

            </tbody>
          </table>
        </div>
      </Suspense>
    </div>
  )
};

export default Homepage;