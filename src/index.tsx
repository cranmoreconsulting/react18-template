import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

if (process.env.NODE_ENV !== 'production') {
    console.log('Looks like we are in development mode!');
}

const container = document.getElementById('root');

// @ts-ignore - TS Not supporting React 18 Root API Changes Yet --TODO Update on Stable 18 Release
ReactDOM.createRoot(container).render(<App />);
