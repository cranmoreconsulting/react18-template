export default {
    colours: {
        "Lavender Gray": "c9cad9",
        "Lavender Blue": "d1d2f9",
        "Baby Blue Eyes": "a3bcf9",
        "Jet": "333232",
        "Black Coral": "5c5d67"
    }
}
