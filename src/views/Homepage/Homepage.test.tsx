import React from 'react';
import { shallow } from '../../tests/test-setup';
import Homepage from "./index";

describe('Testing Homepage Component', () => {
  const container = shallow(<Homepage />);

  it('Should match the snapshot', () => {
    expect(container.html()).toMatchSnapshot();
  });
});